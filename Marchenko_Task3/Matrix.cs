﻿using System;

namespace Matrix
{
    class Matrix
    {
        private int size;
        private double[,] matrix;

        public Matrix(int size)
        {
            this.size = size;
            this.matrix = new double[size, size];
            for (int i = 0; i < size; ++i) { for (int j = 0; j < size; ++j) this.matrix[i,j] = 0.0d; } 

        }
        public Matrix(Matrix rhs)
        {
            this.size = rhs.size;
            this.matrix = new double[size, size];
            for (int i = 0; i < size; ++i) { for (int j = 0; j < size; ++j) this.matrix[i, j] = rhs[i, j]; }

        }

        public void Fill_Matrix()
        {
            double read;
            for (int i = 0; i < size; ++i)
            {
                for (int j = 0; j < size; ++j)
                {
                    Console.Write("[{0},{1}] =", i + 1, j + 1);
                    read = Convert.ToDouble(Console.ReadLine());
                    this[i, j] = read;
                }
                    
            }

        }
        public void Print()
        {
            
            for (int i = 0; i < size; ++i)
            {
                for (int j = 0; j < size; ++j) Console.Write(this.matrix[i, j] + "  ");
                Console.WriteLine("\n");
            }
        }
        public double this[int i,int j]
        {
            get
            {
                return this.matrix[i, j];
            }
            set
            {
                this.matrix[i, j] = value;
            }
        }
        public static Matrix operator+(Matrix first,Matrix second)
        {
            try
            {
                if(first.size != second.size)
                {
                    throw new Exception("Can't add matrix");
                }

                Matrix res = new Matrix(first.size);

                for (int i = 0; i < res.size; ++i)
                {
                    for (int j = 0; j < res.size; ++j)
                        res[i, j] = first[i, j] + second[i, j];
                }

                return res;
            }
            catch(Exception exc)
            {
                throw exc;
            }
        }

        public static Matrix operator*(Matrix first, Matrix second)
        {
            try
            {
                if (first.size != second.size)
                {
                    throw new Exception("Can't multiply matrix");
                }
                Matrix res = new Matrix(first.size);


                for (var i = 0; i < res.size; ++i)
                {
                    for (var j = 0; j < res.size; ++j)
                    {
                        for (var k = 0; k < res.size; ++k)
                        {
                            res[i, j] += first[i, k] * second[k, j];
                        }
                    }
                }
                return res;
            }
            catch(Exception exp)
            {
                throw exp;
            }
        }

        public Matrix Pow(int power)
        {
            Matrix res;
            if (power == 0)
            {
                res = new Matrix(size);
                for (int i = 0; i < size; ++i) res[i, i] = 1;
                return res;
            }
            res = new Matrix(this);
     
            for(int i =1;i < power;++i)
            {
                res *= this;
             
            }
            return res;
        }
    }
}
