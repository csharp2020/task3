﻿using System;
using Matrix;

namespace  Console_Programm
{
    class Program
    {
        static void Main(string[] args)
        {
            int size;
            int k;
            Console.Write("Size = ");
            size = Convert.ToInt32(Console.ReadLine());

            Console.Write("K = ");
            k = Convert.ToInt32(Console.ReadLine());

            Matrix.Matrix A = new Matrix.Matrix(size);

            Console.WriteLine("Fill matrix: ");
            A.Fill_Matrix();

            Console.WriteLine("Your Matrix: ");
            A.Print();

            Matrix.Matrix Res = new Matrix.Matrix(size);
            for(int i = k;i >=0;--i)
            {
                Res += A.Pow(i);
            }
            Console.WriteLine("Result: ");
            Res.Print();

        }
    }
}
